import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../screens/SplashScreen';
import Register from '../screens/Register';
import Login from '../screens/Login';
import Intro from '../screens/Intro';
import HomePage from '../screens/HomePage';
import ReactNative from '../screens/ReactNative';
import Profile from '../screens/Profile';
import Maps from '../screens/Maps';
import Chat from '../screens/Chat';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Stack = createStackNavigator();

const HomeStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="HomePage"
      component={HomePage}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="ReactNative"
      component={ReactNative}
      options={{headerTitle: 'React Native'}}
    />
  </Stack.Navigator>
);

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomePage"
      tabBarOptions={{
        activeTintColor: '#088dc4',
      }}>
      <Tab.Screen
        name="HomePage"
        component={HomeStack}
        options={{
          headerShown: false,
          tabBarLabel: 'Home',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Maps"
        component={Maps}
        options={{
          headerShown: false,
          tabBarLabel: 'Maps',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="map" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Chat"
        component={Chat}
        options={{
          headerShown: false,
          tabBarLabel: 'Chat',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="chat" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          headerShown: false,
          tabBarLabel: 'Profile',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const UnAuthStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Register"
      component={Register}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="TabNavigator"
      component={TabNavigator}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const AppNavigation = () => {
  const [isLoading, setIsLoading] = React.useState(true);

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <UnAuthStack />
    </NavigationContainer>
  );
};

export default AppNavigation;
