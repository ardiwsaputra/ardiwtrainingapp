import React, {useState, useEffect} from 'react';
import {View, Text, Image, StatusBar, ActivityIndicator} from 'react-native';
import styles from '../../style/styles';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

const slides = [
  {
    key: 1,
    title: 'Belajar Intensif',
    text:
      '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
    image: require('../../assets/images/working-time.png'),
  },
  {
    key: 2,
    title: 'Teknologi Populer',
    text: 'Menggunakan bahasa pemrograman populer',
    image: require('../../assets/images/research.png'),
  },
  {
    key: 3,
    title: 'From Zero to Hero',
    text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
    image: require('../../assets/images/venture.png'),
  },
  {
    key: 4,
    title: 'Training Gratis',
    text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
    image: require('../../assets/images/money-bag.png'),
  },
];

const Intro = ({navigation}) => {
  const [firstLaunch, setFirstLaunch] = useState(null);
  const [loading, setLoading] = useState(null);
  useEffect(() => {
    setLoading(true);
    AsyncStorage.getItem('alreadyLaunched')
      .then((value) => {
        if (value == null) {
          setFirstLaunch(true);
        } else {
          setFirstLaunch(false);
          navigation.reset({
            index: 0,
            routes: [{name: 'Login'}],
          });
        }
      })
      .catch(() => {
        setFirstLaunch(null);
      });
    setLoading(false);
  }, []);

  if (loading) {
    return <ActivityIndicator size="large" />;
  }
  //menampilkan data slides kedalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image style={styles.image} source={item.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };

  //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
  const onDone = async () => {
    try {
      await AsyncStorage.setItem('alreadyLaunched', 'true');
      navigation.navigate('Login');
    } catch (err) {
      console.log('saveToken -> err', err);
    }
  };

  //mengcustom tampilan button done
  const renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  //mengcustom tampilan next button
  const renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <View style={{flex: 1}}>
        {/* merender atau menjalankan library react-native-app-intro-slider */}
        <AppIntroSlider
          data={slides}
          onDone={onDone}
          renderItem={renderItem}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderNextButton}
          keyExtractor={(item, index) => index.toString()}
          activeDotStyle={{backgroundColor: '#191970'}}
        />
      </View>
    </View>
  );
};

export default Intro;
