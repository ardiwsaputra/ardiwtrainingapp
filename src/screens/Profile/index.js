import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Image, Button} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import AsyncStorage from '@react-native-community/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        console.log('getToken -> token', token);
      } catch (err) {
        console.log('getToken -> err', err);
      }
    };
    getToken();
    getCurrentUser();
  }, [userInfo]);

  const getCurrentUser = async () => {
    const userInfo = await GoogleSignin.signInSilently();
    console.log('getCurrentUser -> userInfo', userInfo);
    setUserInfo(userInfo);
  };

  const onLogoutPress = async () => {
    try {
      if (userInfo) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      await AsyncStorage.removeItem('token');
      navigation.reset({
        index: 0,
        routes: [{name: 'Login'}],
      });
    } catch (err) {
      console.log('onLogoutPress -> err', err);
    }
  };

  return (
    <View style={{flex: 1}}>
      <View style={styles.backgroundStyle}>
        <Image
          style={styles.profileImage}
          source={
            userInfo !== null
              ? {uri: userInfo && userInfo.user && userInfo.user.photo}
              : require('../../assets/images/profile.jpg')
          }
        />
        <Text style={{color: Colors.white, fontWeight: 'bold', fontSize: 18}}>
          {userInfo !== null
            ? userInfo && userInfo.user && userInfo.user.name
            : 'Ardi W (JWT & FP)'}
        </Text>
      </View>
      <View style={styles.backgroundStyleTwo}></View>
      <View style={styles.containerStyle}>
        <View style={styles.contentStyle}>
          <Text style={styles.textContent}>Tanggal lahir</Text>
          <Text style={styles.textContent}>15 Maret 1998</Text>
        </View>
        <View style={styles.contentStyle}>
          <Text style={styles.textContent}>Jenis Kelamin</Text>
          <Text style={styles.textContent}>Laki-laki</Text>
        </View>
        <View style={styles.contentStyle}>
          <Text style={styles.textContent}>Hobi</Text>
          <Text style={styles.textContent}>Ngoding dan Traveling</Text>
        </View>
        <View style={styles.contentStyle}>
          <Text style={styles.textContent}>No. Telp</Text>
          <Text style={styles.textContent}>081254333436</Text>
        </View>
        <View style={styles.contentStyle}>
          <Text style={styles.textContent}>Email</Text>
          <Text style={styles.textContent}>
            {userInfo !== null
              ? userInfo && userInfo.user && userInfo.user.email
              : 'ardiwidyanto15@gmail.com'}
          </Text>
        </View>
        <View style={styles.button}>
          <Button
            title="LOGOUT"
            color="#3EC6FF"
            onPress={() => onLogoutPress()}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundStyle: {
    height: '33.3%',
    width: '100%',
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
  },
  backgroundStyleTwo: {
    height: '66.6%',
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  containerStyle: {
    padding: 15,
    borderRadius: 10,
    height: '35%',
    width: '90%',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    position: 'absolute',
    top: '30%',
    left: '5%',
    right: '5%',
    bottom: 0,
  },
  contentStyle: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  textContent: {
    fontSize: 12,
    color: Colors.dark,
  },
  profileImage: {
    width: 86,
    height: 86,
    borderRadius: 50,
    marginTop: '10%',
    marginBottom: '5%',
  },
  button: {
    width: '100%',
    marginTop: 10,
  },
});

export default Profile;
