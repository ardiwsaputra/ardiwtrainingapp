import React, {useState, useEffect} from 'react';
import {
  View,
  StatusBar,
  Image,
  TextInput,
  Button,
  Text,
  ActivityIndicator,
} from 'react-native';
import styles from '../../style/styles';
import AsyncStorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
  title: 'Authentication Required',
  imgColor: '#191970',
  imgErrorColor: 'red',
  sensorDescription: 'Tempelkan Sidik Jari',
  sensorErrorDescription: 'Sidik Jari tidak Terbaca',
  cancelText: 'Cancel',
};

const Login = ({navigation}) => {
  const [email, setEmail] = useState('tes@hello.com');
  const [password, setPassword] = useState('123456');
  const [loading, setLoading] = useState(true);

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
      navigation.reset({
        index: 0,
        routes: [{name: 'TabNavigator'}],
      });
    } catch (err) {
      console.log('saveToken -> err', err);
    }
  };

  useEffect(() => {
    AsyncStorage.getItem('token').then((value) => {
      if (value != null) {
        navigation.reset({
          index: 0,
          routes: [{name: 'TabNavigator'}],
        });
      }
    });
    configureGoogleSignIn();
    setLoading(false);
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '50398892776-ljg5fldn1ajiurbbbo53s2153kmnlnmb.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      navigation.reset({
        index: 0,
        routes: [{name: 'TabNavigator'}],
      });
    } catch (err) {
      console.log('signInWithGoogle -> err', err);
    }
  };

  // const onLoginPress = () => {
  //   const data = {email: email, password: password};
  //   Axios.post(`${api}/login`, data, {
  //     timeout: 2000,
  //   })
  //     .then((res) => {
  //       saveToken(res.data.token);
  //       navigation.navigate('Profile');
  //     })
  //     .catch((err) => {
  //       console.log('Login -> err', err);
  //     });
  // };

  const onLoginPress = async () => {
    try {
      const res = await auth().signInWithEmailAndPassword(email, password);
      saveToken(res.user.uid);
    } catch (err) {
      console.log('onLoginPress -> err', err);
    }
  };

  const signInWithFingerPrint = () => {
    TouchID.authenticate('', config)
      .then(() => {
        navigation.reset({
          index: 0,
          routes: [{name: 'TabNavigator'}],
        });
      })
      .catch(() => {
        alert('Authentication Failed!');
      });
  };

  if (loading) {
    return <ActivityIndicator size="large" />;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <View style={styles.slide}>
        <Image
          style={styles.image}
          source={require('../../assets/images/logo.png')}
        />
        <TextInput
          style={styles.textInput}
          onChangeText={(email) => setEmail(email)}
          value={email}
          placeholder="Email"
        />
        <TextInput
          style={styles.textInput}
          onChangeText={(password) => setPassword(password)}
          value={password}
          placeholder="Password"
          secureTextEntry
        />
        <View style={styles.button}>
          <Button
            title="LOGIN"
            color="#3EC6FF"
            onPress={() => onLoginPress()}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 10,
            width: '90%',
          }}>
          <View style={styles.line} />
          <View>
            <Text style={{width: 50, textAlign: 'center'}}>OR</Text>
          </View>
          <View style={styles.line} />
        </View>
        <View style={{width: '93%', marginTop: 10}}>
          <GoogleSigninButton
            onPress={() => signInWithGoogle()}
            style={{width: '100%', height: 40}}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
          />
        </View>
        <View style={styles.button}>
          <Button
            title="SIGN IN WITH FINGERPRINT"
            color="#191970"
            onPress={() => signInWithFingerPrint()}
          />
        </View>
        <Text style={{textAlign: 'center', marginTop: 30, color: '#a4a4a6'}}>
          Belum mempunyai Akun?{' '}
          <Text
            onPress={() => navigation.navigate('Register')}
            style={{color: '#191970'}}>
            Buat Akun
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default Login;
