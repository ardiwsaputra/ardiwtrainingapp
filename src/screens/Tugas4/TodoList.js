import React, {useContext} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableHighlight,
  FlatList,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import {RootContext} from '../Tugas4';

const TodoList = () => {
  const state = useContext(RootContext);

  return (
    <>
      <StatusBar barStyle="light-content" />
      <View style={styles.container}>
        <Text style={styles.headerTextStyle}>Masukan Todolist</Text>
        <View style={styles.textInputButtonStyles}>
          <TextInput
            style={{
              height: 50,
              width: '85%',
              borderColor: 'black',
              borderWidth: 1,
            }}
            onChangeText={(text) => state.handleChangeInput(text)}
            value={state.input}
            placeholder="Input Here"
          />
          <TouchableHighlight
            onPress={() => state.addTodo()}
            style={{
              justifyContent: 'center',
              alignContent: 'center',
              padding: 10,
              backgroundColor: 'grey',
            }}>
            <Icon name="plus" size={25} color="#ffff" />
          </TouchableHighlight>
        </View>
        <FlatList
          data={state.todos}
          keyExtractor={(item) => item.date}
          renderItem={({item, index}) => (
            <View style={styles.item}>
              <View>
                <Text style={styles.title}>
                  {moment.unix(item.date).format('DD/MM/YYYY')}
                </Text>
                <Text style={styles.title}>{item.title}</Text>
              </View>
              <TouchableHighlight
                onPress={() => state.removeTodo(index)}
                style={{
                  justifyContent: 'center',
                  alignContent: 'center',
                  padding: 10,
                }}>
                <Icon name="trash-o" size={25} color="grey" />
              </TouchableHighlight>
            </View>
          )}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: Colors.white,
  },
  headerTextStyle: {
    color: Colors.dark,
    marginBottom: 5,
  },
  textInputButtonStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    borderColor: 'grey',
    borderWidth: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 16,
  },
});

export default TodoList;
