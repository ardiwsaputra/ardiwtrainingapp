import React, {useState, createContext} from 'react';
import TodoList from './TodoList';
import moment from 'moment';

export const RootContext = createContext();

const Provider = RootContext.Provider;

const Context = () => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);

  handleChangeInput = (value) => {
    setInput(value);
  };

  addTodo = () => {
    const today = moment().unix();
    setTodos((state) => [{title: input, date: today}, ...state]);
    setInput('');
  };

  removeTodo = (index) => {
    let _todos = todos.filter((_item, _index) => _index !== index);
    setTodos(_todos);
  };

  return (
    <Provider value={{input, todos, handleChangeInput, addTodo, removeTodo}}>
      <TodoList />
    </Provider>
  );
};

export default Context;
