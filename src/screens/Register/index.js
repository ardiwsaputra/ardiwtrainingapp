import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  StyleSheet,
  Image,
  Button,
  TextInput,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import storage from '@react-native-firebase/storage';

const Register = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(options);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = async (uri) => {
    const sessionId = new Date().getTime();
    try {
      await storage().ref(`images/${sessionId}`).putFile(uri);
      alert('Upload Success');
    } catch (e) {
      console.log('uploadImage -> e', e);
      alert('Upload Failed');
    }
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            type={type}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}>
            <View style={{flex: 1, alignSelf: 'flex-start', padding: 15}}>
              <TouchableOpacity
                onPress={() => toggleCamera(camera)}
                style={styles.buttonCircle}>
                <MaterialCommunity name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 75,
              }}>
              <View style={styles.round} />
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}>
              <TouchableOpacity
                onPress={() => takePicture(camera)}
                style={styles.capture}>
                <Icon name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <View style={{flex: 1}}>
      <View style={styles.backgroundStyle}>
        <Image
          style={styles.profileImage}
          source={
            photo
              ? {uri: photo.uri}
              : require('../../assets/images/profile.jpg')
          }
        />
        <TouchableOpacity onPress={() => setIsVisible(true)}>
          <Text style={{color: Colors.white, fontWeight: 'bold', fontSize: 18}}>
            Change picture
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.backgroundStyleTwo}></View>
      <View style={styles.containerStyle}>
        <View style={styles.contentStyle}>
          <Text style={styles.textContent}>Nama</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={(nama) => setNama(nama)}
            value={nama}
            placeholder="Nama"
          />
          <Text style={styles.textContent}>Email</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={(email) => setEmail(email)}
            value={email}
            placeholder="Email"
          />
          <Text style={styles.textContent}>Password</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={(password) => setPassword(password)}
            value={password}
            placeholder="Password"
            secureTextEntry
          />
        </View>
        <View style={styles.button}>
          <Button
            title="REGISTER"
            color="#3EC6FF"
            onPress={() => uploadImage(photo.uri)}
          />
        </View>
      </View>
      {renderCamera()}
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundStyle: {
    height: '33.3%',
    width: '100%',
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
  },
  backgroundStyleTwo: {
    height: '66.6%',
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  containerStyle: {
    padding: 15,
    borderRadius: 10,
    height: '54%',
    width: '90%',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    position: 'absolute',
    top: '30%',
    left: '5%',
    right: '5%',
    bottom: 0,
  },
  contentStyle: {
    padding: 5,
  },
  textContent: {
    fontSize: 14,
    color: Colors.dark,
    fontWeight: 'bold',
  },
  profileImage: {
    width: 86,
    height: 86,
    borderRadius: 50,
    marginTop: '10%',
    marginBottom: '5%',
  },
  button: {
    width: '100%',
    padding: 5,
  },
  textInput: {
    borderColor: '#a4a4a6',
    borderWidth: 0.5,
    borderRadius: 2,
    padding: 10,
    width: '100%',
    marginTop: 5,
    marginBottom: 15,
  },
  round: {
    borderWidth: 5,
    borderColor: '#fff',
    borderRadius: 80,
    margin: 20,
    width: 200,
    height: 300,
  },
  capture: {
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 100,
    margin: 20,
    width: 100,
    height: 100,
  },
  buttonCircle: {
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 100,
  },
});

export default Register;
