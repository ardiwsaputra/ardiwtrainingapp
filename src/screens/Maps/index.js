import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import styles from '../../style/styles';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoiYXJkaXdzYXB1dHJhIiwiYSI6ImNrZzdqcWswajAyMmMycnBydnU3NzBoNjUifQ.0eeohb1pWatJ0UwMTg5Fpg',
);

const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.89869],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.60918, -6.898013],
];

const Maps = ({navigation}) => {
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log(error);
      }
    };
    getLocation();
  }, []);
  return (
    <View style={styles.container}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true}></MapboxGL.UserLocation>
        <MapboxGL.Camera followUserLocation={true}></MapboxGL.Camera>
        {coordinates.map((item, index) => {
          return (
            <MapboxGL.PointAnnotation
              key={index}
              id="pointAnnotation"
              coordinate={item}>
              <MapboxGL.Callout
                title={'Longtitude: ' + item[0] + ' Latitude: ' + item[1]}
              />
            </MapboxGL.PointAnnotation>
          );
        })}
      </MapboxGL.MapView>
    </View>
  );
};

export default Maps;
