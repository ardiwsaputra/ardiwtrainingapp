import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
  TouchableHighlight,
  FlatList,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

const TodoList = () => {
  const [value, onChangeText] = React.useState('');
  const [todoData, setTodo] = React.useState([]);

  const addTodoList = (newTodo) => setTodo((state) => [newTodo, ...state]);
  const removeTodoList = (index) => {
    let _todoData = todoData.filter((_item, _index) => _index !== index);
    setTodo(_todoData);
  };
  return (
    <>
      <StatusBar barStyle="light-content" />
      <View style={styles.container}>
        <Text style={styles.headerTextStyle}>Masukan Todolist</Text>
        <View style={styles.textInputButtonStyles}>
          <TextInput
            style={{
              height: 50,
              width: '85%',
              borderColor: 'black',
              borderWidth: 1,
            }}
            onChangeText={(text) => onChangeText(text)}
            value={value}
            placeholder="Input Here"
          />
          <TouchableHighlight
            onPress={() => addTodoList({date: moment().unix(), title: value})}
            style={{
              justifyContent: 'center',
              alignContent: 'center',
              padding: 10,
              backgroundColor: 'grey',
            }}>
            <Icon name="plus" size={25} color="#ffff" />
          </TouchableHighlight>
        </View>
        <FlatList
          data={todoData}
          keyExtractor={(item) => item.date}
          renderItem={({item, index}) => (
            <View style={styles.item}>
              <View>
                <Text style={styles.title}>
                  {moment.unix(item.date).format('DD/MM/YYYY')}
                </Text>
                <Text style={styles.title}>{item.title}</Text>
              </View>
              <TouchableHighlight
                onPress={() => removeTodoList(index)}
                style={{
                  justifyContent: 'center',
                  alignContent: 'center',
                  padding: 10,
                }}>
                <Icon name="trash-o" size={25} color="grey" />
              </TouchableHighlight>
            </View>
          )}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: Colors.white,
  },
  headerTextStyle: {
    color: Colors.dark,
    marginBottom: 5,
  },
  textInputButtonStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    borderColor: 'grey',
    borderWidth: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 16,
  },
});

export default TodoList;
