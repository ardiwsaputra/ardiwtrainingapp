import React from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';

const HomePage = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.textWrapper}>
          <Text style={styles.myText}>Kelas</Text>
          <View style={styles.iconWrapper}>
            <TouchableOpacity
              onPress={() => navigation.navigate('ReactNative')}>
              <View style={styles.rowContentWrapper}>
                <MaterialCommunityIcons
                  name="react"
                  color={'#ffffff'}
                  size={40}
                />
                <Text style={styles.contentRowText}>React Native</Text>
              </View>
            </TouchableOpacity>

            <View style={styles.rowContentWrapper}>
              <MaterialCommunityIcons
                name="database"
                color={'#ffffff'}
                size={36}
              />
              <Text style={styles.contentRowText}>Data Science</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <MaterialCommunityIcons
                name="react"
                color={'#ffffff'}
                size={40}
              />
              <Text style={styles.contentRowText}>React JS</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <MaterialCommunityIcons
                name="laravel"
                color={'#ffffff'}
                size={40}
              />
              <Text style={styles.contentRowText}>Laravel</Text>
            </View>
          </View>
        </View>
        <View style={styles.textWrapper}>
          <Text style={styles.myText}>Kelas</Text>
          <View style={styles.iconWrapper}>
            <View style={styles.rowContentWrapper}>
              <MaterialCommunityIcons
                name="wordpress"
                color={'#ffffff'}
                size={40}
              />
              <Text style={styles.contentRowText}>Wordpress</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Image
                style={styles.image}
                source={require('../../assets/images/website-design.png')}
              />
              <Text style={styles.contentRowText}>Design Grafis</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <MaterialCommunityIcons
                name="server"
                color={'#ffffff'}
                size={40}
              />
              <Text style={styles.contentRowText}>Web Server</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Image
                style={styles.image}
                source={require('../../assets/images/ux.png')}
              />
              <Text style={styles.contentRowText}>UI/UX</Text>
            </View>
          </View>
        </View>
        <View style={styles.textWrapper}>
          <Text style={styles.myText}>Summary</Text>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>React Native</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>20 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>Data Science</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>30 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>React JS</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>66 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>Laravel</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>60 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>Wordpress</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>20 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>Design Grafis</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>20 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>Web Server</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>20 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.iconWrapperRow}>
            <Text style={styles.myText}>UI/UX</Text>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Today</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>20 Orang</Text>
            </View>
          </View>
          <View style={styles.textWraperRow}>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>Total</Text>
            </View>
            <View style={styles.rowContentWrapper}>
              <Text style={styles.contentRowText}>100 Orang</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default HomePage;
