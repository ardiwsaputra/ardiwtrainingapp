import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    padding: 10,
    alignItems: 'center',
  },
  textWrapper: {
    backgroundColor: '#088dc4',
    width: wp('94.5%'),
    borderRadius: 5,
    marginBottom: 10,
  },
  iconWrapper: {
    backgroundColor: '#3EC6FF',
    width: wp('94.5%'),
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    flexDirection: 'row',
  },
  iconWrapperRow: {
    backgroundColor: '#3EC6FF',
    width: wp('94.5%'),
    flexDirection: 'row',
  },
  textWraperRow: {
    backgroundColor: '#088dc4',
    width: wp('94.5%'),
    flexDirection: 'row',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  rowContentWrapper: {
    padding: 10,
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  myText: {fontSize: hp('2%'), color: '#ffffff', margin: 10},
  contentRowText: {fontSize: hp('1.7%'), color: '#ffffff', textAlign: 'center'},
  image: {
    width: 40,
    height: 40,
  },
});
