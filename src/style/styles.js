import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    padding: 10,
  },
  buttonCircle: {
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
    backgroundColor: '#191970',
    borderRadius: 100,
  },
  image: {
    width: 200,
    height: 200,
    marginVertical: 20,
  },
  title: {
    fontSize: 24,
    color: '#191970',
    fontWeight: 'bold',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#a4a4a6',
    fontWeight: 'bold',
  },
  textInput: {
    borderColor: '#a4a4a6',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    width: '90%',
    marginTop: 10,
  },
  button: {
    width: '90%',
    marginTop: 10,
  },
  line: {flex: 1, height: 1, backgroundColor: '#a4a4a6'},
});
