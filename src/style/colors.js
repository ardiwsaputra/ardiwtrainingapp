const color = {
  blue: '#3EC6FF',
  darkblue: '#088dc4',
  white: '#ffffff',
  darkred: '#8b0000',
};

export default color;
