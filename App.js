/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import firebase from '@react-native-firebase/app';
import AppNavigation from './src/navigation/navigation';

const firebaseConfig = {
  apiKey: 'AIzaSyCd3xYOEwW2U0cItsekM6NXxhUFTkUkHFw',
  authDomain: 'ardiapp-4fd59.firebaseapp.com',
  databaseURL: 'https://ardiapp-4fd59.firebaseio.com',
  projectId: 'ardiapp-4fd59',
  storageBucket: 'ardiapp-4fd59.appspot.com',
  messagingSenderId: '50398892776',
  appId: '1:50398892776:web:4b6cdc6ef5bfe4f190af5d',
  measurementId: 'G-NS9Q42MLSL',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return <AppNavigation />;
};

export default App;
